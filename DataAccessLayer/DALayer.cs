﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using ModelLayer;

namespace DataAccessLayer
{
    public class DALayer : DbContext
    {
        public DALayer() : base("name=DBConnection")
        {
        }
        public DbSet<TaskModel> Task { get; set; }

    }
}
