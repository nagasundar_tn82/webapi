﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace DataAccessLayer
{
    public class DBLayer
    {
        string connectionString = ConfigurationManager.ConnectionStrings["DBConnection"].ConnectionString;

        public DataSet GetAllTasks()
        {



            SqlDataAdapter da = new SqlDataAdapter("USP_GET_ALLORSEARCHED_TASKS", connectionString);
            da.SelectCommand.CommandType = CommandType.StoredProcedure;
            da.SelectCommand.Parameters.Add("@p_TaskID", SqlDbType.Int).Value = 0;
            DataSet ds = new DataSet();
            da.Fill(ds);
            return ds;


        }

        public DataTable GetSearchTaskID(int TaskID)
        {
            SqlDataAdapter da = new SqlDataAdapter("USP_GET_ALLORSEARCHED_TASKS", connectionString);
            da.SelectCommand.CommandType = CommandType.StoredProcedure;
            da.SelectCommand.Parameters.Add("@p_TaskID", SqlDbType.Int).Value = TaskID;
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            da.Fill(ds);
            if (ds.Tables.Count > 0)
            { dt = ds.Tables[0]; }
            return dt;
        }
        //public int AddTaskDetails(Task taskDetails)
        //{ }
        public int UpdateTaskDetails(Task taskDetails)
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand("USP_INSERTUPDATE_TASK", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    //cmd.Parameters.Add("@FirstName", SqlDbType.VarChar).Value = taskDetails..Text;
                    //cmd.Parameters.Add("@LastName", SqlDbType.VarChar).Value = txtLastName.Text;

                    con.Open();
                    cmd.ExecuteNonQuery();
                }
            }
            return 1;
        }

        public int DeleteTaskDetails(int taskID)
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand("USP_DELETE_TASK", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    //cmd.Parameters.Add("@FirstName", SqlDbType.VarChar).Value = txtFirstName.Text;
                    //cmd.Parameters.Add("@LastName", SqlDbType.VarChar).Value = txtLastName.Text;

                    con.Open();
                    cmd.ExecuteNonQuery();
                }

            }
            return 1;
        }
    }
}
