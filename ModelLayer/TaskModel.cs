﻿using System;
using System.Collections.Generic;
using System.Linq; 
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace ModelLayer
{
    [Table("TASK_TABLE")]
    public class TaskModel
    {
        [Key]
        public int TaskID { get; set; }
        public string TaskName { get; set; }

        public int Parent_ID { get; set; }
        public int Priority { get; set; }
        //        [Column(TypeName = "Date",]
        //[Column(TypeName = "Date"),DataType(DataType.Date)]
        //[DisplayFormat(DataFormatString ="{0:yyyy-MM-dd}", ApplyFormatInEditMode =true)]
        public string StartDate { get; set; }


       // [Column(TypeName = "Date")]
        //[DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        //[Column(TypeName = "Date"), DataType(DataType.Date)]
        public string EndDate { get; set; }

        public bool IsTaskEnd  {get;set;}   
    }
}
