﻿using ModelLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using ModelLayer;

namespace WebAppGet.Controllers
{
    //[EnableCors(origins: "*", methods: "*", headers: "*")]
    //[Route("api/Task")]
    public class AddController : ApiController
    {
       // Task newTask = new Task();
        DataTable dt = new DataTable();
        //[HttpGet]
        //public IHttpActionResult GetTaskID()
        //{

        //    newTask.TaskID = 100;
        //    return Ok(newTask);

        //}

        [HttpGet]
      // [Route("GetSearchTaskID")]
        public IHttpActionResult GetSearchTaskID(int TaskID)
        {

            /* String[] TaskName = { "BHF-1", "BHF-2", "BHF-3", "BHF-4", "BHF-5", "BHF-6" };
             // int[] TaskIDCollection = { 10, 20, 30, 40, 50 };
             //int GetTaskID =  TaskIDCollection.Select(x=>x.)
             // string value = Array.Find(myArr, element => element.IndexOf(TaskID)
             string value = string.Empty;
             try
             {
                 value = TaskName[TaskID];
             }
             catch (Exception exx)
             {
                 value = "Give between 1 and 7";
             }
             finally
             {
                 newTask.TaskName = value;

             }*/
            TaskModel clickedTask = new TaskModel();
            BusinessLayer.BusinessLayer bl = new BusinessLayer.BusinessLayer();
            clickedTask =  bl.GetSearchTaskID(TaskID);
            //clickedTask.StartDate = "2012/12/15";


            return Ok(clickedTask);

        }

        [HttpPost]
       //[Route("AddTaskDetails")]
        public IHttpActionResult AddTaskDetails(TaskModel taskDetails)
        {
            //if (dt.Columns.Count == 0)
            //{
            //    dt.Columns.Add("TaskID", typeof(int));
            //    dt.Columns.Add("TaskName", typeof(string));
            //}

            //dt.Rows.Add(taskDetails.TaskID, taskDetails.TaskName);

            

            BusinessLayer.BusinessLayer bl = new BusinessLayer.BusinessLayer();
            bl.AddTaskDetails(taskDetails);

            return Ok(taskDetails);
        }


        [HttpPut]
        //[Route("UpdateTaskDetails")]
        public IHttpActionResult UpdateTaskDetails(TaskModel taskDetails)
        {
            // if (dt.Columns.Count == 0)
            // {
            //     dt.Columns.Add("TaskID", typeof(int));
            //     dt.Columns.Add("TaskName", typeof(string));
            // }
            // dt.Rows.Add(1, "test");
            //// dt.Rows[taskDetails.TaskID]["TaskName"] = taskDetails.TaskName;
            // dt.Rows[0]["TaskName"] = taskDetails.TaskName;


            BusinessLayer.BusinessLayer bl = new BusinessLayer.BusinessLayer();
            bl.UpdateTaskDetails(taskDetails);
            return Ok(taskDetails);
        }


        [HttpDelete]
       // [Route("DeleteTaskDetails")]
        public IHttpActionResult DeleteTaskDetails(int taskID)
               //public IHttpActionResult DeleteTaskDetails(Task taskID)
        {
            //if (dt.Columns.Count == 0)
            //{
            //    dt.Columns.Add("TaskID", typeof(int));
            //    dt.Columns.Add("TaskName", typeof(string));
            //}
            //dt.Rows.Add(1, "test1");
            //dt.Rows.Add(2, "test2");
            //dt.Rows.Add(3, "test3");
            //dt.Rows.Add(4, "test4");
            //dt.Rows.Add(5, "test5");
            //dt.Rows.Add(6, "test6");

            //dt.Rows.RemoveAt(taskID);


            BusinessLayer.BusinessLayer bl = new BusinessLayer.BusinessLayer();
            bool retValue = bl.DeleteTaskDetails(taskID);


            return Ok(retValue);

            //List<TaskModel> lstTask = new List<TaskModel>();

           
        }


        [HttpGet]
      //  [Route("GetAllTaskDetails")]
        public IHttpActionResult GetAllTaskDetails()
        {

            List<TaskModel> lstTask = new List<TaskModel>();

            BusinessLayer.BusinessLayer bl = new BusinessLayer.BusinessLayer();
            lstTask = bl.GetAllTaskDetails();

            //DateTime dtd = DateTime.Now;
            //dtd.ToShortDateString();


            //foreach (TaskModel mm in lstTask)
            //{
            //    mm.StartDate.Value.ToShortDateString(); 
            //}





            //for (int i = 1; i <= 5; i++)
            //{
            //    Task takeTask = new Task();
            //    takeTask.TaskID = i;
            //    takeTask.TaskName = "BHF-Service " + i;
            //    lstTask.Add(takeTask);
            //}            

            //  lstTask.Add(newTask);
            return Ok(lstTask);
  

        }
    }
}
