﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAppGet.Models
{
    public class Task
    {
        public int TaskID { get; set; }
        public string TaskName { get; set; }
    }
}