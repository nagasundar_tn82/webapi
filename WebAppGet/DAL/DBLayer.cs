﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace WebAppGet.DAL
{
    public class DBLayer
    {
        string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["DBConnection"].ConnectionString;
        public DataSet GetAllTasks()
        {
            //using (SqlConnection con = new SqlConnection(connectionString)) 
            //{
            // using (SqlCommand cmd = new SqlCommand("sp_Add_contact", con)) 
            // {
            //  cmd.CommandType = CommandType.StoredProcedure;

            //  cmd.Parameters.Add("@FirstName", SqlDbType.VarChar).Value = txtFirstName.Text;
            //  cmd.Parameters.Add("@LastName", SqlDbType.VarChar).Value = txtLastName.Text;

            //  con.Open();
            //  cmd.ExecuteNonQuery();
            //}


            SqlDataAdapter da = new SqlDataAdapter("USP_GETALLTASKS", connectionString);
            da.SelectCommand.CommandType = CommandType.StoredProcedure;
            da.SelectCommand.Parameters.Add("@p_TaskID", SqlDbType.Int);
            DataSet ds = new DataSet();
            da.Fill(ds);
            return ds;


        }            

    }
}