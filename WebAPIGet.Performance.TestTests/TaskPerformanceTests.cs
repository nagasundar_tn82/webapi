﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NBench;
using WebAppGet.Controllers;
using WebAppGet;
using ModelLayer;

namespace WebAPIGet.Performance.Test
{
    public class TaskPerformance
    {
        [PerfBenchmark(Description = "Performace Test for GET", RunMode = RunMode.Iterations, TestMode = TestMode.Test)]
        [MemoryAssertion(MemoryMetric.TotalBytesAllocated, MustBe.LessThan, 100000d)]
        [ElapsedTimeAssertion(MaxTimeMilliseconds = 500)]
        [CounterMeasurement("AddController")]
        public void GetAllTasksDetails_Benchmark_Performance_ElaspedTime()
        {
            AddController controller = new AddController();
            controller.GetAllTaskDetails();
        }


        [PerfBenchmark(Description = "Performace Test for ADD", RunMode = RunMode.Iterations, TestMode = TestMode.Test)]
        [MemoryAssertion(MemoryMetric.TotalBytesAllocated, MustBe.LessThan, 100000d)]
        [ElapsedTimeAssertion(MaxTimeMilliseconds = 500)]
        [CounterMeasurement("AddController")]
        public void AddTaskDetails_Benchmark_Performance_ElaspedTime()
        {
            AddController controller = new AddController();
            TaskModel taskDetails = new TaskModel();
            taskDetails.TaskName = "TestingUnit";
            taskDetails.Parent_ID = 5;
            taskDetails.Priority = 4;
            taskDetails.IsTaskEnd = false;
            taskDetails.StartDate = "2019-15-4";
            taskDetails.EndDate = "2019-18-5";
            controller.AddTaskDetails(taskDetails);
        }

        [PerfBenchmark(Description = "Performace Test for UPDATE", RunMode = RunMode.Iterations, TestMode = TestMode.Test)]
        [MemoryAssertion(MemoryMetric.TotalBytesAllocated, MustBe.LessThan, 100000d)]
        [ElapsedTimeAssertion(MaxTimeMilliseconds = 500)]
        [CounterMeasurement("AddController")]
        public void UpdateTaskDetails_Benchmark_Performance_ElaspedTime()
        {
            AddController controller = new AddController();
            AddController tc = new AddController();
            TaskModel taskDetails = new TaskModel();
            taskDetails.TaskName = "TestingUnitUP";
            taskDetails.Parent_ID = 5;
            taskDetails.Priority = 4;
            taskDetails.IsTaskEnd = false;
            taskDetails.StartDate = "2019-15-4";
            taskDetails.EndDate = "2019-18-5";
            taskDetails.TaskID = 14;
            controller.UpdateTaskDetails(taskDetails);
        }


        [PerfBenchmark(Description = "Performace Test for DELETE", RunMode = RunMode.Iterations, TestMode = TestMode.Test)]
        [MemoryAssertion(MemoryMetric.TotalBytesAllocated, MustBe.LessThan, 100000d)]
        [ElapsedTimeAssertion(MaxTimeMilliseconds = 500)]
        [CounterMeasurement("AddController")]
        public void DeleteTaskDetails_Benchmark_Performance_ElaspedTime()
        {
            AddController controller = new AddController();
            controller.DeleteTaskDetails(2019);
        }
    }
}
