﻿using DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModelLayer;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BusinessLayer
{
    public class BusinessLayer
    {
        public List<TaskModel> GetAllTaskDetails()
        {

            //DBLayer dbLay = new DBLayer();
            //return dbLay.GetAllTasks();

            //List<TaskModel> taskList = new List<TaskModel>();
            //taskList = (from DataRow dr in dbLay.GetAllTasks().Tables[0].Rows
            //    select new TaskModel()
            //    {
            //        TaskID = Convert.ToInt32(dr["StudentId"]),   
            //        TaskName = dr["StudentName"].ToString(),   

            //     }).ToList();

            List<TaskModel> taskList = new List<TaskModel>();
            using (DALayer dataAccessLayerInstance = new DALayer())
            {
                return dataAccessLayerInstance.Task.ToList();
            }

        }

        public TaskModel GetSearchTaskID(int TaskID)
        {

            using (DALayer daLayerObject = new DALayer())
            {
                List<TaskModel> allTasks = (from specificTask in daLayerObject.Task
                                       select specificTask).ToList();
                TaskModel task = allTasks.Where(a => a.TaskID == TaskID).FirstOrDefault();
                return task;
            }
        }


        public void AddTaskDetails(TaskModel newTask)
        {
            using (DALayer daLayerObject = new DALayer())
            {
                daLayerObject.Task.Add(newTask);
                daLayerObject.SaveChanges();
            }

        }


        public void UpdateTaskDetails(TaskModel taskToUpdate)
        {
            using (DALayer daLayerObject = new DALayer())
            {
                //TaskModel updateTask = new TaskModel();
                TaskModel updateTask = daLayerObject.Task.FirstOrDefault(a => a.TaskID == taskToUpdate.TaskID);
                //List <TaskModel> updateTaskList = daLayerObject.Task.ToList();
                //updateTask = updateTaskList.FirstOrDefault(a => a.TaskID == taskToUpdate.TaskID);
                //FirstOrDefault(a => a.TaskID == taskToUpdate.TaskID);

                if (updateTask != null)
                {
                    //daLayerObject.Task.Remove(updateTask);
                    //daLayerObject.Task.Add(taskToUpdate);
                    //daLayerObject.Task.
                    updateTask.IsTaskEnd = taskToUpdate.IsTaskEnd;
                    updateTask.Parent_ID = taskToUpdate.Parent_ID;
                    updateTask.Priority = taskToUpdate.Priority;
                    updateTask.StartDate = taskToUpdate.StartDate;
                    updateTask.TaskName = taskToUpdate.TaskName;
                    updateTask.EndDate = taskToUpdate.EndDate;
                    daLayerObject.SaveChanges();
                }
                else
                {
                    throw new Exception(string.Format("Task - {0} not found", taskToUpdate.TaskName));
                }
            }

        }

        public bool DeleteTaskDetails(int id)
        {
            using (DALayer daLayerObject = new DALayer())
            {
                TaskModel taskToDelete = daLayerObject.Task.FirstOrDefault(a => a.TaskID == id);
                if (taskToDelete != null)
                {
                    daLayerObject.Task.Remove(taskToDelete);
                    daLayerObject.SaveChanges();
                    return true;
                }
                else
                {
                    throw new Exception(string.Format("Task - {0} not found", taskToDelete.TaskName));
                }
            }

        }
    }
}