﻿using System;
//using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;
using WebAppGet.Controllers;
using ModelLayer;
using System.Web.Http.Results;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Results;

namespace WebAPIGet.Test
{
    
    [TestFixture]
    public class TaskTest
    {
        [Test]
        public void GetAllTaskDetails_Test()
        {
            AddController tc = new AddController();
            var result = tc.GetAllTaskDetails();
            var actual = result as OkNegotiatedContentResult<List<TaskModel>>;
            Assert.IsNotNull(actual);
            Assert.IsNotNull(actual.Content);
        }

        [Test]
        public void AddTaskDetails_Test()
        {
            TaskModel taskDetails = new TaskModel();
            taskDetails.TaskName = "TestingUnit";
            taskDetails.Parent_ID = 5;
            taskDetails.Priority = 4;
            taskDetails.IsTaskEnd = false;
            taskDetails.StartDate = "2019-15-4";
            taskDetails.EndDate = "2019-18-5";
            AddController tc = new AddController();

            IHttpActionResult result = tc.AddTaskDetails(taskDetails);
            var actual = result as OkNegotiatedContentResult<TaskModel>;
            Assert.IsNotNull(actual);
            Assert.IsNotNull(actual.Content);           
        }

        [Test]
        public void UpdateTaskDetails_Test()
        {
            AddController tc = new AddController();
            TaskModel taskDetails = new TaskModel();
            taskDetails.TaskName = "TestingUnit14";
            taskDetails.Parent_ID = 5;
            taskDetails.Priority = 4;
            taskDetails.IsTaskEnd = false;
            taskDetails.StartDate = "2019-15-4";
            taskDetails.EndDate = "2019-18-5";
            taskDetails.TaskID = 14;
            var result = tc.UpdateTaskDetails(taskDetails);
            var actual = result as OkNegotiatedContentResult<TaskModel>;
            Assert.IsNotNull(actual);
          
        }

        [Test]
        public void DeleteTaskDetails_Test()
        {
            AddController tc = new AddController();
            var result = tc.DeleteTaskDetails(1024);
            var actual = result as OkNegotiatedContentResult<bool>;
            Assert.IsNotNull(actual);
            Assert.IsNotNull(actual.Content);
        }


    }

}
